package com.riggas.core;

import java.util.Arrays;
import java.util.Collections;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.SessionCookieConfig;
import javax.servlet.SessionTrackingMode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;


@SpringBootApplication
@Configuration
//@EnableJpaRepositories(basePackages="riggas.vacons.repository")
@ComponentScan(basePackages = "com.riggas.core")
@ImportResource({ "classpath*:applicationContext.xml" })
public class SpringCustomApplication {

	public static void main(String[] args) {
		System.setProperty("server.port", "80");
		SpringApplication.run(SpringCustomApplication.class, args);
	}

	@Bean
	public DispatcherServlet dispatcherServlet() {
		return new DispatcherServlet();
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet(), "/");
		registration.setLoadOnStartup(1);
		registration.setName(DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_REGISTRATION_BEAN_NAME);
		return registration;
	}

	@Bean
	public ServletContextInitializer servletContextInitializer() {
		return new ServletContextInitializer() {
			@Override
			public void onStartup(ServletContext servletContext) throws ServletException {
				servletContext.setSessionTrackingModes(Collections.singleton(SessionTrackingMode.COOKIE));
				SessionCookieConfig sessionCookieConfig = servletContext.getSessionCookieConfig();
				sessionCookieConfig.setHttpOnly(true);
			}
		};
	}

	@Bean
	public SimpleUrlHandlerMapping faviconHandlerMapping() {
		SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
		mapping.setOrder(Integer.MIN_VALUE);
		mapping.setUrlMap(Collections.singletonMap("static/favicon.ico", faviconRequestHandler()));
		return mapping;
	}

	@Bean
	protected ResourceHttpRequestHandler faviconRequestHandler() {
		ResourceHttpRequestHandler requestHandler = new ResourceHttpRequestHandler();
		requestHandler.setLocations(Arrays.<Resource>asList(new ClassPathResource("/")));
		return requestHandler;
	}
	
	
	
	
	
//	@Bean
//	public FilterRegistrationBean someFilterRegistration() {
//		FilterRegistrationBean registration = new FilterRegistrationBean();
//		registration.setFilter(someFilter());
//		registration.addUrlPatterns("/admin/*");
//		registration.setName("adminFilter");
//		registration.setOrder(1);
//		return registration;
//	}

//	public Filter someFilter() {
//		return new CustomAdminFilter();
//	}
	
	
//	@Bean
//	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
//		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
//		entityManagerFactoryBean.setDataSource(dataSource());
//		entityManagerFactoryBean.setPersistenceProviderClass(HibernatePersistenceProvider.class);
//		entityManagerFactoryBean.setPackagesToScan("riggas.vacons.model");
//		entityManagerFactoryBean.setJpaProperties(hibProperties());
//		return entityManagerFactoryBean;
//	}
//		
//		
//	@Bean
//	public JpaTransactionManager transactionManager() {
//		JpaTransactionManager transactionManager = new JpaTransactionManager();
//		transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
//		return transactionManager;
//	}
//
//	private Properties hibProperties() {
//		Properties properties = new Properties();
//		properties.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
//		properties.put("hibernate.hbm2ddl.auto", "create-drop");
//		properties.put("hibernate.show_sql", false);
//		properties.put("hibernate.format_sql", true);
//		properties.put("hibernate.naming-strategy", "org.hibernate.cfg.ImprovedNamingStrategy");
//		return properties;
//	}
//
//	@Bean
//	public DataSource dataSource() {
////		DriverManagerDataSource dataSource = new DriverManagerDataSource();
//		
////		dataSource.setDriverClassName(("org.h2.Driver"));
////		dataSource.setUrl(("jdbc:h2:mem:datajpa;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE"));
////		dataSource.setUsername("SA");
////		dataSource.setPassword("");
//		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
//		dataSource.setDriverClassName("org.h2.Driver");
//	    dataSource.setUrl("jdbc:h2:mem:datajpa;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE");
//	    dataSource.setUsername("SA");
//	    dataSource.setPassword("");
////	    dataSource.setTestWhileIdle(testWhileIdle);     
////	    dataSource.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMills);
////	    dataSource.setValidationQuery(validationQuery);
//	    dataSource.setMaxActive(30);
//	    dataSource.setMinIdle(10);
//	    dataSource.setMaxIdle(20);
//	    dataSource.setInitialSize(10);
//		return dataSource;
//	}

}